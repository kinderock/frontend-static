import MinigamesApi from '../../../../components/api/MinigamesApi';

class XrollApi extends MinigamesApi {

  constructor () {
    super();
    this.setBaseUrl('/minigames/xroll/api/'); // пишем сюда наш базовый URL к API, от которого будут формироваться остальные URLы
  }

  info () {
    return this.get('info');
  }

  turn (_csrf) {
    return this.post('turn');
  }

  newGame (_csrf) {
    return this.post('close');
  }

  buyGame (_csrf) {
    return this.post('buy-game');
  }

  balance () {
    return this.get('balance');
  }

}

/**
 * пример использования
 * let api = new XrollApi();
 * api.getAllPosts().then((response) => console.log(response));
 */

export default XrollApi;
