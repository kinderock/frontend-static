(function () {
    if (!document.querySelectorAll) {
        document.querySelectorAll = function (selectors) {
            var style = document.createElement('style'), elements = [], element;
            document.documentElement.firstChild.appendChild(style);
            document._qsa = [];

            style.styleSheet.cssText = selectors + '{x-qsa:expression(document._qsa && document._qsa.push(this))}';
            window.scrollBy(0, 0);
            style.parentNode.removeChild(style);

            while (document._qsa.length) {
                element = document._qsa.shift();
                element.style.removeAttribute('x-qsa');
                elements.push(element);
            }
            document._qsa = null;
            return elements;
        };
    }

    if (!document.querySelector) {
        document.querySelector = function (selectors) {
            var elements = document.querySelectorAll(selectors);
            return (elements.length) ? elements[0] : null;
        };
    }
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(searchElement, fromIndex) {
            var k;

            // 1. Положим O равным результату вызова ToObject с передачей ему
            //    значения this в качестве аргумента.
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var O = Object(this);

            // 2. Положим lenValue равным результату вызова внутреннего метода Get
            //    объекта O с аргументом "length".
            // 3. Положим len равным ToUint32(lenValue).
            var len = O.length >>> 0;

            // 4. Если len равен 0, вернём -1.
            if (len === 0) {
                return -1;
            }

            // 5. Если был передан аргумент fromIndex, положим n равным
            //    ToInteger(fromIndex); иначе положим n равным 0.
            var n = +fromIndex || 0;

            if (Math.abs(n) === Infinity) {
                n = 0;
            }

            // 6. Если n >= len, вернём -1.
            if (n >= len) {
                return -1;
            }

            // 7. Если n >= 0, положим k равным n.
            // 8. Иначе, n<0, положим k равным len - abs(n).
            //    Если k меньше нуля 0, положим k равным 0.
            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

            // 9. Пока k < len, будем повторять
            while (k < len) {
                // a. Положим Pk равным ToString(k).
                //   Это неявное преобразование для левостороннего операнда в операторе in
                // b. Положим kPresent равным результату вызова внутреннего метода
                //    HasProperty объекта O с аргументом Pk.
                //   Этот шаг может быть объединён с шагом c
                // c. Если kPresent равен true, выполним
                //    i.  Положим elementK равным результату вызова внутреннего метода Get
                //        объекта O с аргументом ToString(k).
                //   ii.  Положим same равным результату применения
                //        Алгоритма строгого сравнения на равенство между
                //        searchElement и elementK.
                //  iii.  Если same равен true, вернём k.
                if (k in O && O[k] === searchElement) {
                    return k;
                }
                k++;
            }
            return -1;
        };
    }
    //addEventListener polyfill 1.0 / Eirik Backer / MIT Licence
    (function(win, doc){
        if(win.addEventListener)return;		//No need to polyfill

        function docHijack(p){var old = doc[p];doc[p] = function(v){return addListen(old(v))}}
        function addEvent(on, fn, self){
            return (self = this).attachEvent('on' + on, function(e){
                var e = e || win.event;
                e.preventDefault  = e.preventDefault  || function(){e.returnValue = false}
                e.stopPropagation = e.stopPropagation || function(){e.cancelBubble = true}
                fn.call(self, e);
            });
        }
        function addListen(obj, i){
            if(i = obj.length)while(i--)obj[i].addEventListener = addEvent;
            else obj.addEventListener = addEvent;
            return obj;
        }

        addListen([doc, win]);
        if('Element' in win)win.Element.prototype.addEventListener = addEvent;			//IE8
        else{		//IE < 8
            doc.attachEvent('onreadystatechange', function(){addListen(doc.all)});		//Make sure we also init at domReady
            docHijack('getElementsByTagName');
            docHijack('getElementById');
            docHijack('createElement');
            addListen(doc.all);
        }
    })(window, document);
    var BrowserDetect = {
        init: function () {
            var info = this.searchString(this.dataBrowser) || {identity:"unknown"};
            this.browser = info.identity;
            this.version = this.searchVersion(navigator.userAgent)
                || this.searchVersion(navigator.appVersion)
                || "an unknown version";
            this.platformInfo = this.searchString(this.dataPlatform) || this.dataPlatform["unknown"];
            this.platform = this.platformInfo.identity;
            var browserInfo = this.urls[this.browser];
            if (!browserInfo) {
                browserInfo = this.urls["unknown"];
            } else if (browserInfo.platforms) {
                var info = browserInfo.platforms[this.platform];
                if (info) {
                    browserInfo = info;
                }
            }
            this.urls = browserInfo;
        },
        searchString: function (data) {
            for (var i = 0; i < data.length; i++){
                var info = data[i];
                var dataString = info.string;
                var dataProp = info.prop;
                this.versionSearchString = info.versionSearch || info.identity;
                if (dataString) {
                    if (dataString.indexOf(info.subString) !== -1) {
                        var shouldExclude = false;
                        if (info.excludeSubstrings) {
                            for (var ii = 0; ii < info.excludeSubstrings.length; ++ii) {
                                if (dataString.indexOf(info.excludeSubstrings[ii]) !== -1) {
                                    shouldExclude = true;
                                    break;
                                }
                            }
                        }
                        if (!shouldExclude)
                            return info;
                    }
                } else if (dataProp) {
                    return info;
                }
            }
        },
        searchVersion: function (dataString) {
            var index = dataString.indexOf(this.versionSearchString);
            if (index === -1) {
                return;
            }
            return parseFloat(dataString.substring(
                index + this.versionSearchString.length + 1));
        },
        dataBrowser: [
            { string: navigator.userAgent,
                subString: "Chrome",
                excludeSubstrings: ["OPR/", "Edge/"],
                identity: "Chrome"
            },
            { string: navigator.userAgent,
                subString: "OmniWeb",
                versionSearch: "OmniWeb/",
                identity: "OmniWeb"
            },
            { string: navigator.vendor,
                subString: "Apple",
                identity: "Safari",
                versionSearch: "Version"
            },
            { string: navigator.vendor,
                subString: "Opera",
                identity: "Opera"
            },
            { string: navigator.userAgent,
                subString: "Android",
                identity: "Android"
            },
            { string: navigator.vendor,
                subString: "iCab",
                identity: "iCab"
            },
            { string: navigator.vendor,
                subString: "KDE",
                identity: "Konqueror"
            },
            { string: navigator.userAgent,
                subString: "Firefox",
                identity: "Firefox"
            },
            { string: navigator.vendor,
                subString: "Camino",
                identity: "Camino"
            },
            {// for newer Netscapes (6+)
                string: navigator.userAgent,
                subString: "Netscape",
                identity: "Netscape"
            },
            { string: navigator.userAgent,
                subString: "Edge/",
                identity: "Edge"
            },
            { string: navigator.userAgent,
                subString: "MSIE",
                identity: "Explorer",
                versionSearch: "MSIE"
            },
            { // for IE11+
                string: navigator.userAgent,
                subString: "Trident",
                identity: "Explorer",
                versionSearch: "rv"
            },
            { string: navigator.userAgent,
                subString: "Gecko",
                identity: "Mozilla",
                versionSearch: "rv"
            },
            { // for older Netscapes (4-)
                string: navigator.userAgent,
                subString: "Mozilla",
                identity: "Netscape",
                versionSearch: "Mozilla"
            }
        ],
        dataPlatform: [
            { string: navigator.platform,
                subString: "Win",
                identity: "Windows",
                browsers: [
                    {url: "http://www.mozilla.com/en-US/firefox/new/", name: "Mozilla Firefox"},
                    {url: "http://www.opera.com/", name: "Opera"},
                    {url: "http://www.google.com/chrome/", name: "Google Chrome"},
                    {url: "http://www.microsoft.com/en-us/windows/windows-10-upgrade ", name: "Edge"},
                    {url: "http://www.microsoft.com/ie", name: "Internet Explorer"}
                ]
            },
            { string: navigator.platform,
                subString: "Mac",
                identity: "Mac",
                browsers: [
                    {url: "http://www.mozilla.com/en-US/firefox/new/", name: "Mozilla Firefox"},
                    {url: "http://www.google.com/chrome/", name: "Google Chrome"},
                    {url: "http://www.opera.com/", name: "Opera"},
                    {url: "http://www.webkit.org/", name: "WebKit Developer Builds"}
                ]
            },
            { string: navigator.userAgent,
                subString: "iPhone",
                identity: "iPhone/iPod",
                browsers: [
                    {url: "http://www.mozilla.com/en-US/firefox/new/", name: "Mozilla Firefox"}
                ]
            },
            { string: navigator.platform,
                subString: "iPad",
                identity: "iPad",
                browsers: [
                    {url: "http://www.mozilla.com/en-US/firefox/new/", name: "Mozilla Firefox"}
                ]
            },
            { string: navigator.userAgent,
                subString: "Android",
                identity: "Android",
                browsers: [
                    {url: "https://market.android.com/details?id=org.mozilla.firefox", name: "Mozilla Firefox"},
                    {url: "https://market.android.com/details?id=com.opera.browser", name: "Opera Mobile"}
                ]
            },
            { string: navigator.platform,
                subString: "Linux",
                identity: "Linux",
                browsers: [
                    {url: "http://www.mozilla.com/en-US/firefox/new/", name: "Mozilla Firefox"},
                    {url: "http://www.google.com/chrome/", name: "Google Chrome"},
                    {url: "http://www.opera.com/", name: "Opera"}
                ]
            },
            { string: "unknown",
                subString: "unknown",
                identity: "unknown",
                browsers: [
                    {url: "http://www.mozilla.com/en-US/firefox/new/", name: "Mozilla Firefox"},
                    {url: "http://www.google.com/chrome/", name: "Google Chrome"},
                    {url: "http://www.opera.com/", name: "Opera"},
                    {url: "http://www.webkit.org/", name: "WebKit Developer Builds"}
                ]
            }
        ],
        /*
        upgradeUrl:         Tell the user how to upgrade their browser.
        troubleshootingUrl: Help the user.
        platforms:          Urls by platform. See dataPlatform.identity for valid platform names.
        */
        urls: {
            "Chrome": {
                upgradeUrl: "http://www.google.com/support/chrome/bin/answer.py?answer=95346",
                troubleshootingUrl: "http://www.google.com/support/chrome/bin/answer.py?answer=1220892"
            },
            "Firefox": {
                upgradeUrl: "http://www.mozilla.com/en-US/firefox/new/",
                troubleshootingUrl: "https://support.mozilla.com/en-US/kb/how-do-i-upgrade-my-graphics-drivers"
            },
            "Opera": {
                platforms: {
                    "Android": {
                        upgradeUrl: "https://market.android.com/details?id=com.opera.browser",
                        troubleshootingUrl: "http://www.opera.com/support/"
                    }
                },
                upgradeUrl: "http://www.opera.com/",
                troubleshootingUrl: "http://www.opera.com/support/"
            },
            "Android": {
                upgradeUrl: null,
                troubleshootingUrl: null
            },
            "Safari": {
                platforms: {
                    "iPhone/iPod": {
                        upgradeUrl: "http://www.apple.com/ios/",
                        troubleshootingUrl: "http://www.apple.com/support/iphone/"
                    },
                    "iPad": {
                        upgradeUrl: "http://www.apple.com/ios/",
                        troubleshootingUrl: "http://www.apple.com/support/ipad/"
                    },
                    "Mac": {
                        upgradeUrl: "http://www.webkit.org/",
                        troubleshootingUrl: "https://support.apple.com/kb/PH21426"
                    }
                },
                upgradeUrl: "http://www.webkit.org/",
                troubleshootingUrl: "https://support.apple.com/kb/PH21426"
            },
            "Explorer": {
                upgradeUrl: "http://www.microsoft.com/ie",
                troubleshootingUrl: "http://msdn.microsoft.com/en-us/library/ie/bg182648(v=vs.85).aspx"
            },
            "Edge": {
                upgradeUrl: "http://www.microsoft.com/en-us/windows/windows-10-upgrade",
                troubleshootingUrl: "http://msdn.microsoft.com/en-us/library/ie/bg182648(v=vs.85).aspx"
            },
            "unknown": {
                upgradeUrl: null,
                troubleshootingUrl: null
            }
        }
    };
    var CookieController = (function () {
        function CookieController() {
        }
        CookieController.readCookie = function (cookie_name) {
            var nameEQ = cookie_name + "=", ca = document.cookie.split(';'), count = ca.length, i, c;
            for (i = 0; i < count; i++) {
                c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        };
        CookieController.getMailByMpopCookie = function () {
            var cookie_name = 'Mpop', mpop = CookieController.readCookie(cookie_name), result = null;
            if (mpop && mpop.length) {
                result = mpop.split(':');
                if (result && result.length > 2) {
                    return result[2];
                }
            }
            return result;
        };
        CookieController.setCookie = function (name, value, options) {
            options = options || {};
            var expires = options.expires;
            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }
            value = encodeURIComponent(value);
            var updatedCookie = name + "=" + value;
            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }
            document.cookie = updatedCookie;
        };
        CookieController.deleteCookie = function (name) {
            CookieController.setCookie(name, "", {
                expires: -1
            });
        };
        return CookieController;
    }());
    var _createNode = function (tag, className, html) {
        var node = document.createElement(tag);
        if (className) {
            node.className = className;
        }
        if (html) {
            node.innerHTML = html;
        }

        return node;
    };
    var get_html = function (browser_html_list) {
        var lang = CookieController.readCookie('cur_language');
        if (!lang) {
            lang = 'ru';
        } else {
            lang = 'en';
        }
        var translate = {
            'ru': {
                'header': 'Ваш браузер устарел',
                'text': 'Вы можете продолжать на свой страх и риск, но мы не можем гарантировать работоспобность приложения. Рекомендуем вам обновить свой браузер по ссылкам ниже.',
                'button': 'Я все понял'
            },
            'en': {
                'header': 'Your browser is outdated.',
                'text': 'You may continue using the application further, but we cannot guarantee you that it will work properly. We highly recommend you to update your browser using the links below.',
                'button': 'I understand'
            }
        };
        var text = translate[lang];
        var old_browser_html = '<div class="old-browser-wrapper">' +
            ' <div class="old-browser-coll">' +
            '        <div class="old-browser-coll__text">'+text.header+'</div>' +
            '        <div class="old-browser-coll__close"></div>' +
            '    </div>' +
            ' <div class="old-browser-popup">' +
            '        <h3 class="old-browser-popup__header">'+text.header+'</h3>' +
            '        <div class="old-browser-popup__important-img"></div>' +
            '        <p class="old-browser-popup__text">'+text.text+'</p>' +
            '        <div class="old-browser-popup__browser-list">' +browser_html_list+
            '        </div>' +
            '        <div class="old-browser-popup__button" >'+text.button+'</div>' +
            '        <div class="old-browser-popup__close"></div>' +
            '    </div>' +
            '</div>';
        return old_browser_html;
    };
    var get_browser_html_list = function (browsers) {
        var i, count = browsers.length, html = '', browser;
        for (i = 0; i < count; i++) {
            browser = browsers[i];
            html += ' <a class="old-browser-popup__browser-link" href="'+browser.url+'" target="_blank">\n' +
                '                <div class="old-browser-popup__browser-image old-browser-popup__browser-image--'+browser.name.toLowerCase()+'"></div>\n' +
                '                <span class="old-browser-popup__browser-name">'+browser.name+'</span>\n' +
                '            </a>';
        }
        return html;
    };
    var get_browser_list = function (platform) {
            var img_url = '/minigames/battlepass_v3/static/wf/build/interfaces/images/old_browser/';
            var browser_list = [
                {
                    "name": "Chrome",
                    "url": "https://www.google.com/chrome/browser/desktop/index.html"
                },
                {
                    "name": "Firefox",
                    "url": "http://www.mozilla.com/en-US/firefox/new/"
                },
                {
                    "name": "Edge",
                    "url": "https://www.microsoft.com/en-us/windows/microsoft-edge"
                }

            ];
            if (platform === 'Mac' ||platform ===  'iPhone/iPod' || platform === 'iPad') {
                var url = 'http://www.webkit.org/';
                if (platform !== 'iPhone/iPod') {
                    url = 'http://www.apple.com/ios/';
                }
                browser_list.push({
                    "name": "Safari",
                    "url": url
                });
            }
            return browser_list;
        };
    var append_window_html = function (html) {
        var node = _createNode("div", '', html);
        document.getElementsByTagName('body')[0].appendChild(node);
    };
    var init_events = function () {
        var small_block = document.querySelector('.old-browser-coll'), popup = document.querySelector('.old-browser-popup');
        var close_all = function () {
            CookieController.setCookie('bp3_old_browser', "1", {
                expires : Math.round(+ new Date() / 1000) + 2592000
            });
            small_block.style.display = 'none';
            popup.style.display = 'none';
        };
        small_block.addEventListener('click', function () {
            popup.style.display = 'block';
        });
        document.querySelector('.old-browser-popup__close').addEventListener("click", close_all);
        document.querySelector('.old-browser-popup__button').addEventListener("click", close_all);
        document.querySelector('.old-browser-coll__close').addEventListener("click", close_all);
    };
    var main = function () {
        var is_need_show_old_browser = true;



        BrowserDetect.init();
        switch (BrowserDetect.browser) {
            case "Chrome":
                if (BrowserDetect.version >= 33) {
                    is_need_show_old_browser = false;

                }
                break;
            case "Firefox":
                if (BrowserDetect.version >= 26) {
                    is_need_show_old_browser = false;

                }
                break;
            case "Edge":
                if (BrowserDetect.version >= 15) {
                    is_need_show_old_browser = false;

                }
                break;
            case "Explorer":
                is_need_show_old_browser = true;
                break;
            case "Android":
                is_need_show_old_browser = false;
                break;
            case "Safari":
                if (BrowserDetect.version >= 8) {
                    is_need_show_old_browser = false;
                }
                break;
            default:
                //do nothing special;
                break;

        }
        if (window.opera) {
            is_need_show_old_browser = true;
        }
        if (!window.WebGLRenderingContext) {
            is_need_show_old_browser = true;
        }
        if (CookieController.readCookie('bp3_old_browser')) {
            is_need_show_old_browser = false;
        }
        is_need_show_old_browser = true;

        if (is_need_show_old_browser) {
            var browser_list = get_browser_list(BrowserDetect.platform);
            var browser_list_html = get_browser_html_list(browser_list);
            var html = get_html(browser_list_html);
            append_window_html(html);
            init_events();
        }
    };
    main();
})();
import styles from './theme.styl';
export default {};