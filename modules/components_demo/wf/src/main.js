import Vue from 'vue';
import App from './App';
import tooltip from '@comp/tooltip/projects/wf/tooltip';
Vue.config.productionTip = false;
import(/* webpackChunkName: "old_browser_plugin" */ '@comp/old_browser/projects/wf/old_browser.js');
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
});