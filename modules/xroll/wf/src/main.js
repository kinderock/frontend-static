import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import i18n from '@comp/translate/translate';
import store from '../../model/store/index.js'
import router from '@/routes/index.js'

Vue.config.productionTip = false;

Vue.use(Vuex)

new Vue({
  el: '#app',
  store,
  router,
  i18n,
  components: { App },
  template: '<App/>',
});
