import axios from 'axios';
import qs from 'qs';
class MinigamesApi {
  constructor () {
    /**
     * @protected
     */
    this.invalid_csrf = false;
    this._axios = this._getAxios();
  }
  setBaseUrl (base_url) {
    this._axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    this._axios.defaults.baseURL = base_url;
  }

  /**
   * @protected
   * @return {AxiosInstance} axios instance
   */
  _getAxios () {
    return axios.create({
      baseURL: 'https://api.example.com'
    });
  }

  userInfo () {
    return new Promise((resolve, reject) => {
      return this._baseCall('get', '/minigames/user/info').then((response) => {
        document.cookie = 'mg_token=' + response.success.data.data.token;
        resolve(response);
      })
    });
  }

  _baseCall (method, url, data, responseType = 'json') {
      let headers = {};
      if (method === 'post') {
        headers['Content-Type'] = 'application/x-www-form-urlencoded';
        data = qs.stringify(data);
      }
      return axios({
        method,
        url,
        data,
        headers,
        responseType
      })
        .then((response) => {
          return {
            success: {
              data: response.data,
              status: response.status
            }
          }
        })
        .catch((error) => {
            return {
              error: error.response.data
            }
        })
    }

   call (method, url, data, responseType = 'json') {
    let headers = {};
    if (method === 'post') {
      headers['Content-Type'] = 'application/x-www-form-urlencoded';
      data = qs.stringify(data);
    }
     return this._axios({
      method,
      url,
      data,
      headers,
      responseType
    })
       .then((response) => {
       return {
         success: {
           data: response.data,
           status: response.status
         }
       }
     })
       .catch((error) => {
       //если токен протух
       if (error.response.data.data.code === 10 && error.response.data.state === 'Fail') {
         //перезапросим токен и вызовем заново исходный метод
         return this.userInfo().then(() => {
           return this.call(method, url, data, responseType);
         });
       } else {
         return {
           error: error.response.data
         }
       }
     })
  }

  get (url, data) {
    return this.call('get', url, data);
  }

  post (url, data) {
    return this.call('post', url, data);

  }


}
export default MinigamesApi;
