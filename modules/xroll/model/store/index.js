import Vue from 'vue'
import Vuex from 'vuex'
import XrollApi from '../api/XrollApi.js'

Vue.use(Vuex)

let api = new XrollApi()

const store = new Vuex.Store({
  state: {
    user: {},
    error_data: null,
    game_data: {},
    game_fields: {},
    user_balance: null,
    turn_resp: {},
    is_loading: false,
    cur_language: null
  },

  getters: {
    getUserInfo(state) {
      return state.user
    },

    getErrorData(state) {
      return state.error_data
    },

    getFullData(state) {
      return state.game_data
    },

    getRecordsOfTheDay(state) {
      return state.game_data.top
    },

    getUserRecord(state) {
      return state.game_data.personal_top
    },

    getGameFields(state) {
      return state.game_fields
    },

    getUserTotalBonus(state) {
      return state.game_data.total_bonus
    },

    getUserFreeGames(state) {
      return state.game_data.free_games
    },

    getWinSector(state) {
      return state.turn_resp.last_turn
    },

    getLooseState(state) {
      return state.game_data.loss
    },

    getUserBalance(state) {
      return state.user_balance
    },

    getUserNewday(state) {
      return state.game_data.new_day
    },

    getUserCredit(state) {
      return state.game_data.is_credit
    },

    getBonusUsed(state) {
      return state.game_data.bonus_used
    },

    getLoading(state) {
      return state.is_loading
    },

    getCurrentTime(state) {
      return state.game_data.currenttime
    },

    getActionEnd(state) {
      return state.game_data.actionend
    }
  },

  mutations: {
    updateUserInfo(state, data) {
      document.cookie = 'mg_token=' + data.success.data.data.token
      state.user = data
    },

    updateGameData(state, newData) {
      let new_game_fields = Object.assign({}, newData.fields)

      if (new_game_fields[6] !== 0) {
        new_game_fields[6] = 'freegame'
      }
      if (new_game_fields[7] !== 0) {
        new_game_fields[7] = 'double'
      }
      if (new_game_fields[8] !== 0) {
        new_game_fields[8] = 'reload'
      }

      state.game_fields = new_game_fields
      state.game_data = newData
    },

    updateUserBalance(state, data) {
      state.user_balance = data.balance
    },

    saveTurnResponse(state, data) {
      state.turn_resp = data
    },

    setLoading(state) {
      state.is_loading = true
    },

    cancelLoading(state) {
      state.is_loading = false
    },

    writeErrorData(state, data) {
      state.error_data = data
    },

    writeCurrentLanguage(state, data) {
      state.cur_language = data
    }
  },

  actions: {
    userInfo({ commit }) {
      return new Promise((resolve, reject) => {
        api.userInfo().then(response => {
          if (response.success) {
            commit('updateUserInfo', response)
            resolve(response)
          } else {
            commit('writeErrorData', response.error.data)
          }
        })
      })
    },

    info({ commit }) {
      return new Promise((resolve, reject) => {
        api.info().then(response => {
          if (response.success) {
            commit('updateGameData', response.success.data.data)
            resolve(response.success.data)
          } else {
            commit('writeErrorData', response.error.data)
          }
        })
      })
    },

    turn({ commit, state }) {
      commit('setLoading')
      return new Promise((resolve, reject) => {
        api.turn().then(response => {
          commit('saveTurnResponse', response.success.data.data)
          commit('cancelLoading')
          resolve(response.success.data.data)
        })
      })
    },

    startNewGame({ commit, state }) {
      api.newGame().then(response => {
        api.info().then(response => {
          commit('updateGameData', response.success.data.data)
        })
      })
    },

    buyNewGame({ commit, state }) {
      api.buyGame().then(response => {
        api.info().then(response => {
          commit('updateGameData', response.success.data.data)
        })
      })
    },

    userBalance({ commit, state }) {
      api.balance().then(response => {
        commit('updateUserBalance', response.success.data.data)
      })
    }
  }
})

export default store
