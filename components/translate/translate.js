import i18next from 'i18next';
import VueI18Next from '@panter/vue-i18next';
import Vue from 'vue';

class i18nIniter {
  /**
   * Инициализируем i18next
  */
  constructor () {
    Vue.use(VueI18Next);

    let domain_zone = window.location.host.split('.').reverse()[0],
        default_language = {
          'ru': 'ru',
          "com": 'en'
        };

    i18next.init({
      lng: this.getCookie('cur_language') || default_language[domain_zone],
      fallbackLng: ['en', 'ru'],
    });
    this.loadTranslations();
  }


  /**
   * loadTranslations() возвращает обьект с переводами
   * @return {locales} json object
  */
  loadTranslations() {
    let language_list = ['en', 'de', 'fr', 'pl', 'ru'];
    let locales = {};

    language_list.forEach((lang) => {
      import(
        /* webpackMode: "eager"*/
        /* webpackChunkName: "translate" */
        `@/locales/${lang}/translation.json`)
        .then((file) => {
          i18next.addResourceBundle(lang, 'translation', file[lang].translation);
        });
    });

    return locales;
  }

  getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));

    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  getLib () {
    return new VueI18Next(i18next);
  }
}

const INITER = new i18nIniter();

export default INITER.getLib();
